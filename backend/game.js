let fs = require('fs')

module.exports = function(app) {
    let allWord = [];
    let tokenWord = {};

    fs.readFile("./backend/dictionary.txt", 'utf8', function(err, data) {
        if (err) throw err;
        allWord = data.split("\r\n");
    });

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    const guid = () => {
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    app.post("/api/getWord", function (req, res) {
        let params = req.body;
        let randomInt = Math.floor(Math.random() * allWord.length);
        let randomWord = allWord[randomInt];
        let newToken = function() {
            let newToken = guid();
            tokenWord[newToken] = {word: randomWord, nbTry: 0};
            res.status(201).send({token: newToken, wordSize: randomWord.length});
        };

        if(!params.token) {
            newToken();
        } else {
            if(tokenWord[params.token]) {
                tokenWord[params.token] = {word: randomWord, nbTry: 0};
                res.status(201).send({wordSize: randomWord.length});
            } else {
                newToken();
            }
        }
    });

    app.post("/api/checkWord", function (req, res) {
        let params = req.body;
        if(tokenWord[params.token]) {
            let answer = tokenWord[params.token].word.toUpperCase();
            let word = params.word;
            let result = new Array(word.length).fill({});
            let win = true;

            //get number of each letter in answer
            let nbLetterAnswer = {};
            for(let i = 0; i < answer.length; ++i) {
                if(nbLetterAnswer[answer[i]]) {
                    nbLetterAnswer[answer[i]] += 1;
                } else {
                    nbLetterAnswer[answer[i]] = 1;
                }
            }

            //get for each letter if she is correcty placed
            let nbLetterWord = {};
            for(let i = 0; i < answer.length; ++i) {
                if(answer[i] === word[i]) {
                    result[i] = {index: i, letter: word[i], type: "correct"};
                    if(nbLetterWord[word[i]]) {
                        nbLetterWord[word[i]] += 1;
                    } else {
                        nbLetterWord[word[i]] = 1;
                    }
                }
            }

            //check if win
            for(let i = 0; i < result.length; ++i) {
                if (!result[i].type) {
                    win = false;
                    break;
                }
            }

            if(!win) {
                // check if wrong placed or if not in word
                // if the word is: test
                // and the user word is: eeee
                // only one 'e' is inclueded and 3 is incorrect
                for(let i = 0; i < answer.length; ++i) {
                    if(!result[i].type) {
                        if(answer.includes(word[i])) {
                            if(nbLetterWord[word[i]]) {
                                if(nbLetterWord[word[i]] >= nbLetterAnswer[word[i]]) {
                                    result[i] = {index: i, letter: word[i], type: "incorrect"};
                                } else {
                                    result[i] = {index: i, letter: word[i], type: "included"};
                                }

                                nbLetterWord[word[i]] += 1;
                            } else {
                                result[i] = {index: i, letter: word[i], type: "included"};
                                nbLetterWord[word[i]] = 1;
                            }
                        } else {
                            result[i] = {index: i, letter: word[i], type: "incorrect"};
                        }
                    }
                }
                tokenWord[params.token].nbTry += 1;
            }

            let giveAnswer = "";
            if(tokenWord[params.token].nbTry === 5) {
                giveAnswer = answer;
            }

            res.status(200).send({error: false, message: "", result: result, win: win, answer: giveAnswer});
        }
        else {
            res.status(200).send({error: true, message: "Token not found", result: [], win: false, answer: ""});
        }
    });
}