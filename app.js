let http = require('http');
let express = require('express');
const path = require("path");
const bodyParser = require('body-parser');

let app = express();

app.engine('pug', require('pug').__express)
app.set('views', __dirname + '/public/view');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, "/")));
app.use(bodyParser.json({limit: '10mb'}));

app.locals.basedir = __dirname;

app.get("/index", function (req, res)
{
    res.render("index");
});

app.get("/", function (req, res)
{
    res.render("index");
});


require('./backend/game.js')(app)

http.createServer(app).listen(8080, function(){
    console.log("Express server listening on port " + 8080);
});
