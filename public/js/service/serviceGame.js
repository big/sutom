app.service('serviceGame', function(serviceXhr){

    this.getWord = function (params)
    {
        let url = '/api/getWord';
        return serviceXhr.post(url, params)
    }

    this.checkWord = function (params)
    {
        let url = '/api/checkWord';
        return serviceXhr.post(url, params)
    }

});
