let app = angular.module('myApp', []);

app.controller("main", ["$scope", "serviceGame", function ($scope, serviceGame) {

    let token = localStorage.getItem('token');
    let actualTry = 0;
    let actualTryWord = "";
    let wordLength = 0;

    let gameBoard = document.getElementById("game-board");

    let submitButton = document.getElementById("submit-word");

    submitButton.onclick = function () {
        checkWord();
    }

    let chekWin = function (response) {
        actualTry++;
        actualTryWord = "";
        if (response.win) {
            alert("You win with " + actualTry + " tries");
            actualTry = 0;
            getWord();
        }
        else
        {
            if (actualTry === 5) {
                alert("You lose! the answer is: " + response.answer);
                actualTry = 0;
                getWord();
            }
        }
    }

    let checkWord = function () {
        if (actualTryWord.length === wordLength) {
            serviceGame.checkWord({token: token, word: actualTryWord}).then(function (response) {
                showTryResult(response.result);
                setTimeout(function () {
                    chekWin(response);
                }, 500);
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    let getWord = function () {
        serviceGame.getWord({token: token})
            .then(function (response) {
                wordLength = response.wordSize;
                if (response.token) {
                    localStorage.setItem('token', response.token);
                    token = response.token;
                }
                generateGameBoard();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    let showActualTryWord = function () {
        let actualTryWordRow = gameBoard.children[actualTry];

        if (actualTryWordRow === undefined) {
            return;
        }

        for (let i = 0; i < wordLength; i++) {
            let letter = actualTryWord[i];
            if (letter === undefined) {
                letter = "";
            }
            actualTryWordRow.children[i].innerText = letter;
        }
    }

    let showTryResult = function (result) {
        for (const letter of result) {
            let cell = gameBoard.children[actualTry].children[letter.index];
            if (letter.type === "correct") {
                cell.classList.add("correct");
            }
            else if (letter.type === "incorrect") {
                cell.classList.add("incorrect");
            }
            else if (letter.type === "included") {
                cell.classList.add("included");
            }
        }
    }

    let generateGameBoard = function () {
        gameBoard.innerHTML = "";
        for (let i = 0; i < 5; i++) {
            let row = document.createElement("div");
            row.classList.add("word-row");

            for (let j = 0; j < wordLength; j++) {
                let cell = document.createElement("div");
                cell.classList.add("word-cell");
                row.appendChild(cell);
            }
            gameBoard.appendChild(row);
        }
    }

    onkeydown = function (e) {
        let key = e.key;
        if (key === "Backspace") {
            actualTryWord = actualTryWord.substring(0, actualTryWord.length - 1);
        }
        else if (key === "Enter") {
            checkWord();
        }
        else if ("abcdefghijklmnopqrstuvwxyzàâçéèêùûîô".includes(key.toLowerCase())) {
            if (actualTryWord.length < wordLength) {
                actualTryWord += key.toUpperCase();
            }
        }

        showActualTryWord();
    }

    getWord();

}]);
